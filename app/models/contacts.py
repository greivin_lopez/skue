#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on May 1, 2012

@author: Greivin Lopez
'''

# Copyright (c) 2012 The Skuë Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# ***** Google App Engine imports *****
from google.appengine.ext import db

__author__ = "Greivin Lopez"
__copyright__ = u"Copyright 2012, The Skuë Project"
__credits__ = ["Greivin Lopez"]
__license__ = "MIT"
__version__ = "1"
__maintainer__ = "Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# Contact
#===============================================================================
class Contact(db.Model):
    '''Google App Engine model implementation to store contact information
 
    This model will use an id provided by the client as a key_name.
    '''
    first_name = db.StringProperty()
    last_name = db.StringProperty()
    mobile_number = db.StringProperty()
    email = db.StringProperty()
 
    @classmethod
    def create(cls, identifier, first_name, last_name, mobile_number, email):
        '''Creates a new contact and writes it to the database
 
        Args:
          first_name: The first name of the contact
          last_name: The last name of the contact
          mobile_number: The person's mobile number
          email: The contact's personal e-mail
 
        Returns:
          The newly created Contact model
        '''
        # Generate a unique identifier for the contact
        contact = Contact.get_or_insert(identifier,
                          first_name=first_name,
                          last_name=last_name,
                          mobile_number=mobile_number,
                          email=email)
        db.put(contact)
        return contact
 
    @classmethod
    def get_by_id(cls, id_value):
        '''Gets the contact associated with the given an id
 
        Args:
          id_value: The unique identification number of the contact.
 
        Returns:
          An instance of Contact model associate to the given id
        '''
        return Contact.get_by_key_name(id_value)
 
    def update(self, values):
        '''Updates this instance of Contact with the given values
 
        Args:
          values: The dictionary of values with the fields to change
        '''
        if values['firstName'] is not None:
            self.first_name = values['firstName']
        if values['lastName'] is not None:
            self.last_name = values['lastName']
        if values['mobile'] is not None:
            self.mobile_number = values['mobile']
        if values['email'] is not None:
            self.email = values['email']
        self.put()
 
    @classmethod
    def delete(cls, id_value):
        '''Deletes the Contact associated to the given id from the datastore.
 
        Args:
          id_value: The unique identification number of the contact.
        '''
        contact = Contact.get_by_key_name(id_value)
        if contact is not None:
            db.delete(contact)
