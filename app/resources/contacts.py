#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on May 1, 2012

@author: Greivin Lopez
'''

# Copyright (c) 2012 The Skuë Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# ***** Application modules *****
from app.models.contacts import Contact
from app.response.json.contact import ContactJSON
from skue.rest.api import ResourceDescription
from skue.rest.api import HttpMethodDescription
from skue.rest.api import HttpParameterDescription
from skue.http import CommonResponse
from skue.rest.resources import StoreDocumentResource

__author__ = "Greivin Lopez"
__copyright__ = u"Copyright 2012, The Skuë Project"
__credits__ = ["Greivin Lopez"]
__license__ = "MIT"
__version__ = "1"
__maintainer__ = "Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# ContactResource
#===============================================================================
class ContactResource(StoreDocumentResource):
    '''Represents a contact resource'''
    
    def describe_resource(self):
        """Self description of this resource handler        
        """
        description = "The first name of the contact"
        firstname_parameter = HttpParameterDescription('firstName',
                                                       'string',
                                                       is_required=True,
                                                       description=description)
        
        description = "The last name of the contact"
        lastname_parameter = HttpParameterDescription('lastName',
                                                       'string',
                                                       is_required=True,
                                                       description=description)
        
        description = "The contact's mobile number"
        mobile_parameter = HttpParameterDescription('mobile',
                                                       'string',
                                                       is_required=True,
                                                       description=description)
        
        description = "The contact's e-mail address"
        email_parameter = HttpParameterDescription('email',
                                                       'string',
                                                       is_required=True,
                                                       description=description)
             
        description = "Allow clients to create new contacts"
        put_method = HttpMethodDescription('PUT',
                                            parameters=[
                                                        firstname_parameter,
                                                        lastname_parameter,
                                                        mobile_parameter,
                                                        email_parameter
                                                       ],
                                            description=description)
        
        description = "Retrieve the details of a given contact"
        get_method = HttpMethodDescription('GET',
                                            parameters=[],
                                            description=description)
        
        description = "Represents a contact's information"
        resource = ResourceDescription('Contact',
                                           url="/contacts/<contact-id>",
                                           methods=[put_method, get_method],
                                           description=description)
        return resource
    
    def exists(self, identifier):
        """Gets a value to indicate if the given identifier it's already
        associated to an existing contact.
        
        Args:
          identifier: A value that uniquely identify a contact
        
        Returns:
          True if the identifier is associated to an existing contact or
          False otherwise.
        """
        contact = Contact.get_by_id(identifier)
        return contact is not None
 
    def create_resource(self, identifier):
        """Create a new Contact resource
 
        Returns:
          A resource created response (201).
        """
        first_name = self.payload.get('firstName')
        last_name = self.payload.get('lastName')
        mobile_number = self.payload.get('mobile')
        email = self.payload.get('email')
 
        Contact.create(identifier, first_name, last_name, mobile_number, email)
        return CommonResponse.resource_created(resource_uri=self._new_resource_uri)
    
    def read_resource(self, identifier):
        """Gets a representation of a contact in JSON"""
        contact = Contact.get_by_id(identifier)
        body = ContactJSON(contact)
        return CommonResponse.success(body)
    
    def update_resource(self, identifier):
        contact = Contact.get_by_id(identifier)        
        contact.update(self.payload)
        return CommonResponse.simple_success("The contact was successfully updated.")


        
