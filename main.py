#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Mar 7, 2012

@author: Greivin Lopez
'''

# Copyright (c) 2012 The Skuë Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# ***** Google App Engine imports *****
import webapp2

# ***** Application modules *****
from skue.rest.resources import ApiDocumentationResource
from app.resources.contacts import ContactResource

__author__ = u"Greivin López"
__copyright__ = u"Copyright 2012, The Skuë Project"
__credits__ = [u"Greivin López", u"Esteban Küber"]
__license__ = "MIT"
__version__ = "1"
__maintainer__ = u"Greivin López"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"

API_NAME = "Contacts API"
API_DESCRIPTION = u"An API to handle contacts. Example API to serve as a guideline for Skuë usage."

TASK_HANDLERS = [
                
                ]

API_HANDLERS = [
                   ('/contacts/(.*)', ContactResource)
               ]

API_DOC = [ ('/', ApiDocumentationResource) ]

ROUTES = TASK_HANDLERS + API_HANDLERS + API_DOC

app = webapp2.WSGIApplication(ROUTES, debug=True)

