#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Apr 6, 2012

@author: Greivin Lopez
'''

# Copyright (c) 2012 The Skuë Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# ***** Python built-in modules *****
import os
from types import StringTypes

# ***** Google App Engine modules *****
import webapp2

# ***** Application modules *****
from skue.rest.api import ApiDescription
from skue.rest.api import ResourceOptionsJSONRepresentation
from skue.rest.api import RestApiDocJSONRepresentation
from skue.errors import ResponseError
from skue.http import CommonResponse
from skue.utils import parse_body


__author__ = "Greivin Lopez"
__copyright__ = u"Copyright 2012, The Skuë Project"
__credits__ = ["Greivin Lopez"]
__license__ = "MIT"
__version__ = "1"
__maintainer__ = "Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# RestResource
#===============================================================================
class RestResource(webapp2.RequestHandler):
    """The request handler for RESTful controllers."""
    
    # A dictionary with the parameters of the request regardless of method
    payload = None
    # The list of the required parameters of the request
    required = []
    # The list of the optional parameters of the request
    optional = []
    # String representation of the HTTP method of the current request
    http_method = ''
    # The value of the 'HTTP_USER_AGENT' header
    http_user_agent = ''
    # The RestResourceDescription object that describes this controller options
    resource_description = None
    # The host name of the server
    host_name = None
    # The language the client prefer for the response
    language = 'en'
    # The list of allowed methods
    allowed_methods = ''
    
    def handle_request(self, method, *args, **kwargs):
        """Method that calls the send_response method with the response of
        the resource specific call.

        This function is called with the specific function for the REST
        operation.

        Args:
          method: the method that executes the REST operation
        """
        try:
            self.__entrance()
            # Inject the expected content type for later validation
            kwargs['__content_type__'] = self.content_type
            # Inject the payload to the method arguments
            kwargs.update(self.payload)
            return self.send_response(method(*args, **kwargs))
        except ResponseError, error:
            return self.send_response(error.get_http_response())
        except Exception, error:
            # self.logger.exception('An unexpected error has occur')
            return self.handle_unexpected_error(error)
        
    def __entrance(self):
        """First method executed by every API request

        Will perform common requests stuff such as populating
        internal variables describing the request and loading
        the input parameters and doing automatic validation.
        """
        try:
            # Set the HTTP Method
            self.http_method = self.request.method.upper()
            # Set the host name
            self.host_name = self.request.url.replace(self.request.path, '')
            # Set the user agent            
            try:
                user_agent = os.environ['HTTP_USER_AGENT']
            except KeyError:
                user_agent = None
            if user_agent is not None and isinstance(user_agent, StringTypes):
                self.http_user_agent = user_agent
            
            # Set the intended response representation
            if self.request.headers is not None:
                accepted_format = self.request.headers.get('Accept');
                self.content_type = accepted_format
            
            # Load the parameters sent in the HTTP request
            self.__load_parameters()

        except Exception, error:
            #self.logger.exception('An unexpected error has occur')
            return self.handle_unexpected_error(error)
        
    def __get_payload(self):
        """Populates payload dictionary from method arguments.

        Retrieve the arguments from the HTTP method and populates
        the payload dictionary of this instance.

        Returns:
          A dict structure with the HTTP request arguments and its values
        """
        payload = {};

        if self.http_method in ['put']:
            payload = parse_body(self.request.body)
        else:
            arguments = self.request.arguments()
            if arguments is not None:
                for argument in arguments:
                    payload[argument] = self.request.get(argument)
        return payload

    def __load_parameters(self):
        """Load the parameters of the request.

        Validates the parameters to find missed required parameters or
        unexpected parameters received.

        Raises:
          ParameterMissedError: One of the required parameters is not present
          NotExpectedParameterError: One of the received parameters was not registered
        """
        # Get payload function populates the payload dictionary
        self.payload = self.__get_payload()
        for argument, value in self.payload.items():
            setattr(self, argument, value)
                        
    def options_for_resource(self, *args, **kwargs):
        """Retrieves the information related the communication options
        associated to a particular resource
        
        Returns:
          A self description of the resource in JSON representation        
        """
        response_body = ResourceOptionsJSONRepresentation(self.resource_description)
        return CommonResponse.options(self.get_allowed_methods(), response_body)
    
    def get_allowed_methods(self):
        """Returns a string with the list of HTTP methods allowed for 
        this resource. Needs to be overriden by inheritors."""
        return self.allowed_methods
        
    def send_response(self, handler_response):
        """Writes an output to the API client with the given response

        Args:
          handler_response: An instance of HandlerHttpResponse that contains
          the data for the response.
        """        
        self.response.set_status(handler_response.status_code)
        for key, value in handler_response.headers.iteritems():
            self.response.headers[key] = value
        self.response.out.write(handler_response.write_body())
        
    def handle_unexpected_error(self, error):
        """Handles the given unexpected error.

        Unexpected error is something that should return a 500 status code
        and also write to log the context and error details.

        Args:
          error: The unexpected error to handle.

        Returns:
          An HTTP 500 error response if not in development environment.
        """
        # return self.error(500)
        raise error
    
    #===========================================================================
    # The HTTP Method Handlers
    #===========================================================================
    def get(self, *args, **kwargs):
        """Handler implementation of an HTTP GET method."""
        self.handle_request(self.read_resource, *args, **kwargs)

    def options(self, *args, **kwargs):
        """Handler implementation of an HTTP OPTIONS method.
        """        
        self.handle_request(self.options_for_resource, *args, **kwargs)

    def post(self, *args, **kwargs):
        """Handler implementation of an HTTP POST method."""
        self.handle_request(self.create_resource, *args, **kwargs)

    def put(self, *args, **kwargs):
        """Handler implementation of an HTTP PUT method."""
        self.handle_request(self.update_resource, *args, **kwargs)

    def delete(self, *args, **kwargs):
        """Handler implementation of an HTTP DELETE method."""
        self.handle_request(self.delete_resource, *args, **kwargs)
        
    #===========================================================================
    # Methods for inheritors
    #===========================================================================
    def create_resource(self, *args, **kwargs):
        """This method should allow users to create new resources.

        Should be implemented by inheritors of this class to create
        new instances of the resource.
        """
        return CommonResponse.method_not_allowed(self.get_allowed_methods())

    def read_resource(self, *args, **kwargs):
        """Retrieve the information of the current resource.
        """
        return CommonResponse.method_not_allowed(self.get_allowed_methods())

    def update_resource(self, *args, **kwargs):
        """Updates the current resource instance.
        """
        return CommonResponse.method_not_allowed(self.get_allowed_methods())

    def delete_resource(self, *args, **kwargs):
        """Removes the resource associated with the given value.
        """
        return CommonResponse.method_not_allowed(self.get_allowed_methods())


#===============================================================================
# DocumentResource
#===============================================================================
class DocumentResource(RestResource):
    """Represents a Document resource. A Document resource is an archetype
    to represent a singular concept.
    Individual items that could be part of a Collection resource.
    """
    pass

#===============================================================================
# StoreDocumentResource
#===============================================================================
class StoreDocumentResource(RestResource):
    """Represents a Document resource. A Document resource is an archetype
    to represent a singular concept.
    
    The store document differs from the regular document resource because
    it represents individual elements that are part of a Store resource
    so there are some differences in the way this kind of resource should
    handle the HTTP methods.
    
    Inheritors MUST handle the exists method to indicate if there is
    an existing resource with a provided identification value in order
    to the object to create itself or return it's representation.
    """
    # Will store the URI for a newly created item
    _new_resource_uri = ""
    
    @property
    def new_resource_uri(self):
        """Return the newly created resource URI"""
        return self._new_resource_uri
    
    def put(self, *args, **kwargs):
        """Handler implementation of an HTTP PUT method."""
        if len(args) > 0:
            identifier = args[0]
            if self.exists(identifier):
                # update an existing resource
                super(StoreDocumentResource, self).handle_request(self.update_resource, *args, **kwargs)
            else:
                # create a new resource
                self._new_resource_uri = self.request.path
                super(StoreDocumentResource, self).handle_request(self.create_resource, *args, **kwargs)
                
                
    def exists(self, identifier):
        """Gets a value to indicate if the given identifier it's already
        associated to an existing resource.
        This method MUST be implemented for inheritors of this class.
        
        Args:
          identifier: A value that uniquely identify a resource
        
        Returns:
          True if the identifier is associated to an existing resource or
          False otherwise.
        """
        return False

#===============================================================================
# CollectionResource
#===============================================================================
class CollectionResource(RestResource):
    """Represents a Collection resource. A Collection resource is a list of
    items or other resources handled by the Server.
    """
    # Will store the offset and limit for pagination in GET requests
    _offset = 0
    _count = 100
    
    @property
    def offset(self):
        """Return the starting point when retrieving results in a GET"""
        return self._offset
    
    @property
    def count(self):
        """Return the maximum number of items to return results in a GET"""
        return self._count
        
    def get(self, *args, **kwargs):
        """Handler implementation of an HTTP GET method."""
        self._offset = int(self.request.get("offset", default_value="0"))
        self._count  = int(self.request.get("count", default_value="100"))
        super(CollectionResource, self).handle_request(self.read_resource, *args, **kwargs)


#===============================================================================
# StoreResource
#===============================================================================
class StoreResource(RestResource):
    """Represents an Store resource. An Store resource is a collection of
    resources handled by the client.
    """        
    def post(self, *args, **kwargs):
        """Handler implementation of an HTTP POST method."""
        super(StoreResource, self).handle_request(self.post_not_allowed, *args, **kwargs)
        
    def post_not_allowed(self, *args, **kwargs):
        """Returns a response to indicate that the POST method should not
        be allowed on resources of type Store"""
        return CommonResponse.method_not_allowed(self.get_allowed_methods())

        
#===============================================================================
# ControllerResource
#===============================================================================
class ControllerResource(RestResource):
    """Represents a Controller resource. A Controller resource is a model
    to the concept of a procedure. Some action that is going to be executed
    over other resources but doesn't quite map to the CRUD model.
    """
    def post(self, *args, **kwargs):
        """Handler implementation of an HTTP POST method."""
        super(ControllerResource, self).handle_request(self.execute, *args, **kwargs)
        
    def execute(self, *args, **kwargs):
        """Performs the execution of the controller's action"""
        return CommonResponse.method_not_allowed(self.get_allowed_methods())


#===============================================================================
# ApiDocumentationResource
#===============================================================================
class ApiDocumentationResource(webapp2.RequestHandler):
    """A new kind of resource that allows you to self-document your entire API"""
    
    #===========================================================================
    # The HTTP Method Handlers
    #===========================================================================
    def get(self, *args, **kwargs):
        """Handler implementation of an HTTP GET method."""
        return self.send_response(CommonResponse.method_not_allowed('OPTIONS'))

    def options(self, *args, **kwargs):
        """Handler implementation of an HTTP OPTIONS method.
        """        
        self.create_api_documentation(*args, **kwargs)

    def post(self, *args, **kwargs):
        """Handler implementation of an HTTP POST method."""
        return self.send_response(CommonResponse.method_not_allowed('OPTIONS'))

    def put(self, *args, **kwargs):
        """Handler implementation of an HTTP PUT method."""
        return self.send_response(CommonResponse.method_not_allowed('OPTIONS'))

    def delete(self, *args, **kwargs):
        """Handler implementation of an HTTP DELETE method."""
        return self.send_response(CommonResponse.method_not_allowed('OPTIONS'))
    
    def send_response(self, handler_response):
        """Writes an output to the API client with the given response

        Args:
          handler_response: An instance of HandlerHttpResponse that contains
          the data for the response.
        """        
        self.response.set_status(handler_response.status_code)
        for key, value in handler_response.headers.iteritems():
            self.response.headers[key] = value
        self.response.out.write(handler_response.write_body())
    
    def create_api_documentation(self, *args, **kwargs):
        """Creates the API documentation and return it to consumers"""
        import main
        resources = []
        for web_handler in main.API_HANDLERS:
            web_handler_instance = web_handler[1]()
            if isinstance(web_handler_instance, RestResource):
                resources.append(web_handler_instance.describe_resource())
            del web_handler_instance
        
        api_name = main.API_NAME if main.API_NAME else 'API Name Unknown'
        api_description = main.API_DESCRIPTION if main.API_DESCRIPTION else 'No description provided'
        
        api_documentation = ApiDescription(name=api_name,
                                           resources=resources,
                                           description=api_description)
               
        response_body = RestApiDocJSONRepresentation(api_documentation)
        return self.send_response(CommonResponse.options('OPTIONS', response_body))

