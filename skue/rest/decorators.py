#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 21, 2013

@author: Greivin Lopez
'''

# Copyright (c) 2012 The Skuë Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# ***** Python built-in modules *****

# ***** Google App Engine modules *****

# ***** Application modules *****
from skue.errors import ParameterMissedError, NotAcceptableError

__author__ = "Greivin Lopez"
__copyright__ = u"Copyright 2012, The Skuë Project"
__credits__ = ["Greivin Lopez"]
__license__ = "MIT"
__version__ = "1"
__maintainer__ = "Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# ensure
#===============================================================================
def ensure(required=[], optional=[]):
    """A decorator to ensure the decorated function received
    all the required parameters and also deletes not expected ones.
    
    @ensure(required=['title', 'timeScope'])
    def create_resource(self, **arguments):
        title = arguments['title']
        time_scope = arguments['timeScope']
        ...
    """
    def decorator(func):
        def wrapper(*__args,**__kw):
            # Create the needed sets to work with
            required_set = set(required)
            optional_set = set(optional)
            received_set = set(__kw.keys())
    
            # Validate required parameters
            fail_required = required_set - received_set
            if fail_required:
                for expected_parameter in fail_required:
                    raise ParameterMissedError(parameter=expected_parameter)
    
            # Find any invalid params and remove them from self.payload
            valid_set = required_set.union(optional_set)
            invalid_params = received_set - valid_set
    
            for param in invalid_params:
                if param != '__content_type__':
                    del __kw[param]

            return func(*__args,**__kw)
        return wrapper
    return decorator


#===============================================================================
# response
#===============================================================================
def response(representations=[]):
    """A decorator to indicate which formats a certain method accept for its
    response value"""
    def decorator(func):
        def wrapper(*__args,**__kw):
            if __kw is not None and '__content_type__' in __kw:
                if __kw['__content_type__'] is not None:
                    if not '*/*' in __kw['__content_type__'] and not __kw['__content_type__'] in representations:
                        raise NotAcceptableError(representations)
                    del __kw['__content_type__']
            return func(*__args,**__kw)
        return wrapper
    return decorator
